Currency conversion chart using React.js

The data is consumed from https://api.exchangeratesapi.io/.


To run the project,
1. npm install
2. npm start

Demo: http://currencyconversiondemo.s3-website-ap-southeast-1.amazonaws.com/
