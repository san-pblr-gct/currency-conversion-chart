import React, { Component } from "react";
import fx from "money";

const RateField = props => <div className={props.id}>{props.value}</div>;

class RateTable extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      currencies: []
    };
  }

  componentDidMount() {
    fetch("https://api.exchangeratesapi.io/latest")
      .then(response => {
        return response.json();
      })
      .then(data => {
        fx.base = data.base;
        fx.rates = data.rates;
        let rates = data.rates;
        rates[data.base] = 1;
        let currencyCodes = [];
        for (const key in rates) {
          let fxConverted = [];
          let fxStraight = [];
          for (const fxkey in rates) {
            fxConverted.push(
              fx(1)
                .from(key)
                .to(fxkey)
                .toFixed(2)
            );
            fxStraight.push(
              fx(1)
                .from(fxkey)
                .to(key)
                .toFixed(2)
            );
          }

          currencyCodes.push({
            key,
            inverse: fxConverted,
            straight: fxStraight
          });
        }
    

        this.setState({ currencies: currencyCodes });
      });
  }

  render() {
    return (
      <div>
        <div id="headerGrid">
          <div />
          {this.state.currencies.map(item => (
            <RateField id="headerDiv" value={item.key} />
          ))}
        </div>
        {this.state.currencies.map(ulitem => (
          <div id="headerGrid">
            <RateField id="sideheaderDiv" value={"1" + ulitem.key + "="} />
            {ulitem.inverse.map(value => (
              <RateField id="valuesDiv" value={value} />
            ))}
            <RateField id="sideheaderDiv" value="Inv:" />
            {ulitem.straight.map(value => (
              <RateField id="valuesDiv" value={value} />
            ))}
            <br />
          </div>
        ))}
      </div>
    );
  }
}

export default RateTable;
